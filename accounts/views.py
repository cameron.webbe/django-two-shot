from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth.models import User
# Create your views here.

def signup(request):
    if request.method == "POST":
        f = UserCreationForm(request.POST)
        if f.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            login(request, user)
            return redirect('home')
    else:
        f = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': f})